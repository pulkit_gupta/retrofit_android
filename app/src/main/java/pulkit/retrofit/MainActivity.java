package pulkit.retrofit;

import android.nfc.Tag;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import pulkit.retrofit.API.GitAPI;
import pulkit.retrofit.Model.Data;
import pulkit.retrofit.Model.Data_Response;
import pulkit.retrofit.Model.GitModel;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class MainActivity extends ActionBarActivity {
    private static final String TAG = MainActivity.class.getSimpleName();

    Button btn_Rest;
    TextView tv_response;
    EditText et_restVar;

    RestAdapter restAdapter;
    GitAPI git;


    String API = "https://api.github.com";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //GUI initialization
        btn_Rest = (Button) findViewById(R.id.bt_Rest);
        tv_response = (TextView) findViewById(R.id.tv_response);
        et_restVar = (EditText) findViewById(R.id.et_restVar);

        //RESTAPI
        //Retrofit section start from here...
        restAdapter = new RestAdapter.Builder()
                .setEndpoint(API).build();
        //create an adapter for retrofit with base url

        //creating a service for adapter with our GET class
        git = restAdapter.create(GitAPI.class);


        //Now ,we need to call for response
        //Retrofit using gson for JSON-POJO conversion


        btn_Rest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "Button is clicked");


                git.post(new Data(Integer.parseInt(et_restVar.getText().toString())), new Callback<Data_Response>() {
                    @Override
                    public void success(Data_Response model, Response response) {

                        //we get json object from github server to our POJO or model class
                        //tv_response.setText("Github Name :" + model.getName() + "\nWebsite :" + model.getBlog() + "\nCompany Name :" + model.getCompany());

                        tv_response.setText("Value returned is " + String.valueOf(model.getID()));


                    }

                    @Override
                    public void failure(RetrofitError error) {
                        tv_response.setText(error.getMessage());

                        // disable progressbar
                        //pbar.setVisibility(View.INVISIBLE);

                    }

                    //pbar.setVisibility(View.INVISIBLE);
                });


            }
        });
    }

    @Override
    public void onBackPressed() {
        Log.d(TAG, "closed app. Good bye");
        android.os.Process.killProcess(android.os.Process.myPid());
    }
}