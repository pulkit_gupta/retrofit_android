package pulkit.retrofit.API;

import pulkit.retrofit.Model.Data;
import pulkit.retrofit.Model.Data_Response;
import pulkit.retrofit.Model.GitModel;
import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Path;

/**
 * Created by pulkitgupta on 25/06/2015.
 */
public interface GitAPI {

    @GET("/users/{user}")      //here is the other url part.best way is to start using /
    public void getFeed(@Path("user") String user, Callback<GitModel> response);     //string user is for passing values from edittext for eg: user=basil2style,google
    //response is the response from the server which is now in the POJO

    @POST("/api/post/product")
    public void post (@Body Data model, Callback <Data_Response> cb);

}