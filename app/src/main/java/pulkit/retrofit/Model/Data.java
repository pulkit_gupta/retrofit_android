package pulkit.retrofit.Model;

/**
 * Created by pulkitgupta on 26/07/2015.
 */


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data {
    @Expose
    private Integer price;
    /**
     *
     * @return
     * The price
     */
    public Integer getPrice() {
        return price;
    }

    /**
     *
     * @param price
     * The price
     */
    public void setPrice(Integer price) {
        this.price = price;
    }

    public Data(Integer price){
        this.price = price;
    }

}