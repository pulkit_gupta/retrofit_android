package pulkit.retrofit.Model;

/**
 * Created by pulkitgupta on 26/07/2015.
 */


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data_Response {


    @Expose
    private Integer ID;

    /**
     *
     * @return
     * The price
     */
    public Integer getID() {
        return ID;
    }

    /**
     *
     * @param ID
     * The price
     */
    public void setID(Integer ID) {
        this.ID = ID;
    }

}